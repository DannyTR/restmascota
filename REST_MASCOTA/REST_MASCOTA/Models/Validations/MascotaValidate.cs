﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_MASCOTA.Models
{
    [MetadataType(typeof(Mascota.MetaData))]
    public partial class Mascota
    {
        sealed class MetaData
        {
            [Key]
            public int IdMascota;

            [Required(ErrorMessage = "Ingrese el nombre de la mascota")]
            public string NombreMascota;

            [Required(ErrorMessage = "Ingrese la raza")]
            public string RazaMascota;

            [Required]
            [Range(0, 25, ErrorMessage = "Edad entre 0 y 25")]
            public Nullable<int> EdadMascota;

            [Required(ErrorMessage = "Ingrese numero dee ficha")]
            public Nullable<int> NumeroFicha;

            [Required(ErrorMessage = "Ingrese el nombre del veterinario")]
            public string Vet;
        }
    }
}